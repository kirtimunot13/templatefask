from app import app
from flask import render_template

@app.route('/')
@app.route('/index')

def index():
    user={'username':'kirti'}
    posts=[
        {
            'author':{'username':'Priyanka'},
            'body':"Django Framework"
         },
        {
            'author': {'username': 'Sharlin'},
            'body': "Flask Framework"
        }]
    return render_template("index.html",title="Home",user=user,posts=posts)
    
